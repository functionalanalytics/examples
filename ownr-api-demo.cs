using System;
using System.IO;
using System.Net;
using System.Text;

class Program
{
    static void Main()
    {
        // Specify the URL you want to send the POST request to
        string url = "https://rapidemo-examples.infinity.ownr.io/";

        // Read in the XML payload from a local text file
        string xmlPayload = "{\"fun\":\"echo\", \"args\":{\"n\":10}}";

        // Convert the XML payload to bytes
        byte[] data = Encoding.UTF8.GetBytes(xmlPayload);

        // Create the HttpWebRequest object
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

        // Set the request method to POST
        request.Method = "POST";

        // Set the content type
        request.ContentType = "application/JSON";

        // Set the content length
        request.ContentLength = data.Length;

        // Add basic authorization header
        request.Headers[HttpRequestHeader.Authorization] = "Basic apikey1";

        // Get the request stream and write the data to it
        using (Stream stream = request.GetRequestStream())
        {
            stream.Write(data, 0, data.Length);
        }

        try
        {
            // Get the response from the server
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                // Read the response data
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    Console.WriteLine(result);
                }
            }
        }
        catch (WebException webEx)
        {
            if (webEx.Response is HttpWebResponse errorResponse)
            {
                Console.WriteLine("Error: Response code is not 200 (OK). Received code: " + (int)errorResponse.StatusCode);
            }
            else
            {
                Console.WriteLine("WebException: " + webEx.Message);
            }
        }

        Console.ReadLine();
    }
