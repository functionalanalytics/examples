import json
import requests

app_name = 'apidemo'
api_key = 'apidemokey'
url = 'https://demo.ownr.io/' + app_name + '/'
function_call = {'fun': 'echo', 'args': {'n': 2}}

response = requests.post(url=url, headers={'Authorization': 'Basic ' + api_key}, data=json.dumps(function_call))

print(response.text)