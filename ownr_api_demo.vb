Imports System.Net
Imports System.IO
Imports System.Text

Module Module1
    Sub Main()

        ' Specify the URL you want to send the POST request to
        Dim url As String = "https://rapidemo-examples.infinity.ownr.io/"

        ' Read in the XML for this example
        Dim xmlPayload As String = "{""fun"":""echo"", ""args"":{""n"":10}}"

        ' Convert the XML payload to bytes
        Dim data As Byte() = Encoding.UTF8.GetBytes(xmlPayload)

        ' Create the HttpWebRequest object
        Dim request As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)

        ' Set the request method to POST
        request.Method = "POST"

        ' Set the content type
        request.ContentType = "text/XML"

        ' Set the content length
        request.ContentLength = data.Length

        ' Add basic authorization header
        request.Headers(HttpRequestHeader.Authorization) = "Basic apikey"

        ' Get the request stream and write the data to it
        Using stream As Stream = request.GetRequestStream()
            stream.Write(data, 0, data.Length)
        End Using

        Try
            ' Get the response from the server
            Dim response As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)

            ' Read the response data
            Using reader As New StreamReader(response.GetResponseStream())
                Dim result As String = reader.ReadToEnd()
                Console.WriteLine(result)
            End Using

            response.Close()

        Catch webEx As WebException
            If TypeOf webEx.Response Is HttpWebResponse Then
                Dim errorResponse As HttpWebResponse = DirectCast(webEx.Response, HttpWebResponse)
                Console.WriteLine("Error: Response code is not 200 (OK). Received code: " & CInt(errorResponse.StatusCode))
            Else
                Console.WriteLine("WebException: " & webEx.Message)
            End If
        End Try

        Console.ReadLine()

    End Sub

End Module
